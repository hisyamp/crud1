<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    public function create(){
    	return view('posts.create');
    }

    public function store(Request $request){
    	// $request->validate([
    	// 	"name" => 'required',
    	// 	"title" => 'required:posts',
    	// 	"value" => 'required'
    	// ]);
    	$query = DB::table('questions')->insert([
    		"title" => $request["title"],
    		"value" => $request["question"]
    	]);

    	return redirect('/posts/create')->with('success','Question Dikirimkan!');
    }
    public function index(){
    	$posts = DB::table('questions')->get();
    	
    	return view('posts.index',compact('posts'));
    }
     public function show($id){
    	$post = DB::table('questions')->WHERE ('id', $id)->first();
    	// dd($post);
    	return view('posts.show',compact('post'));
    }
    public function edit($id){
    	$post = DB::table('questions')->WHERE ('id', $id)->first();
    	// dd($post);
    	return view('posts.edit',compact('post'));
    }
    public function update($id,Request $request){
    	// $request->validate([
    	// 	"name" => 'required',
    	// 	"title" => 'required:posts',
    	// 	"value" => 'required'
    	// ]);
    	$query = DB::table('questions')->WHERE ('id', $id)
    	->update([
    		'title' => $request['title'],
    		'value' => $request['value']
    	]);
    	// dd($post);
    	return redirect('/posts/index')->with('succes','Update Berhasil');
    	}
    public function destroy($id){
    $post = DB::table('questions')->WHERE ('id', $id)->delete();
    	// dd($post);
    return redirect('posts/index')->with('success','Pertanyaan Berhasil Dihapus! ');
    }
};

