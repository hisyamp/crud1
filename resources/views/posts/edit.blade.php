@extends('layouts.master')

@section('content')
    
  <div class="ml-3 mt-3">
		<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Question</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/posts/{{$post->id}}" method="post">
                @csrf
                @method('PUT')
                <div class="ml-3 mt-3" mt>
                  
                <!-- <div class="card-body">
                  
                  </div> -->
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" value=" {{ old('title', $post->title) }} " id="title" name="title" placeholder="Question Title">
                    @error('title')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  </div>
                  <div class="form-group">
                    <label for="question">Question</label>
                    <input type="text" class="form-control" value="{{ old('value', $post->value) }}" id="question" name="value" placeholder="Question">
                    @error('question')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                  
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
  </div>
@endsection