@extends('layouts.master')

@section('content')
	<div class="ml-3 mt-3">
       	<a href="/posts/create" class="btn btn-primary">Create Question</a>
		<div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Pertanyaan</h3>
              <!-- /.card-header -->
              @if(session('success'))
              <div class="alert alert-success">
              	{{ session('success') }}
              @endif
              </div>
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Title</th>
                      <th>Question</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    @forelse( $posts as $key => $post )
                    	<tr>
                    		<td>{{ $key + 1 }}</td>
                    		<td>{{ $post -> title }}</td>
                    		<td>{{ $post -> value }}</td>
                    		<td>
                    			<a href="/posts/{{ $post-> id }}" class="btn btn-info btn-sm">show</a>
                    			<a href="/posts/{{ $post-> id }}/edit" class="btn btn-default btn-sm">edit</a>
                    			<form action="/posts/{{ $post-> id }}" method="post">
                    			@csrf
                    			@method('DELETE')
                    			<input type="submit" value="delete" class="btn btn-danger btn-sm">	
                    			</form>
                    		</td>
                    	</tr>
                    	@empty
                    	<tr>
                    		<td colspan="4" align="center">No Post</td>
                    	</tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
		
	</div>
@endsection